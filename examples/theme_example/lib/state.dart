import 'package:flutter/cupertino.dart';

class GlobalState {
  GlobalState._();

  static final showPerformanceOverlay = ValueNotifier(false);
}
