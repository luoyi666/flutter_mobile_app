import 'package:flutter_mobile_app/flutter_mobile_app.dart';
import 'package:web_admin/pages/home.dart';

GoRouter initRouter() {
  return GoRouter(
    initialLocation: '/',
    routes: [
      GoRoute(path: '/', builder: (context, state) => const HomePage()),
    ],
  );
}
