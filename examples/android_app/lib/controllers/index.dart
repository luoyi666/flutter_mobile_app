import 'package:android_app/controllers/theme.dart';

import '../global.dart';
import 'global.dart';

void initController() {
  Get.put(GlobalController());
  Get.put(ThemeController());
}
