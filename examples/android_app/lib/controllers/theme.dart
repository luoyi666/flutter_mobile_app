import 'package:android_app/global.dart';
import 'package:flutter/material.dart';

class ThemeController extends GetxController {
  static ThemeController of = Get.find();
  final themeMode = ThemeMode.system.obs;
  final theme = FlutterThemeData.theme.obs;
  final darkTheme = FlutterThemeData.darkTheme.obs;
  final config = FlutterConfigData(fontWeight: FontWeight.w500).obs;
}
