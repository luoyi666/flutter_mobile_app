import 'package:android_app/global.dart';
import 'package:flutter/scheduler.dart' as scheduler;

class GlobalController extends GetxController {
  static GlobalController of = Get.find();
  late final isLogin = useLocalObs(false, 'isLogin');
  final count = 0.obs;
  final timeDilation = 1.0.obs;

  @override
  void onInit() {
    super.onInit();
    ever(timeDilation, (v) {
      scheduler.timeDilation = v;
    });
  }
}
