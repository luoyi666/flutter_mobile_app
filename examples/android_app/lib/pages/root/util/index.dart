import 'package:flutter/material.dart';

import '../../../global.dart';
import 'animation_test.dart';
import 'async_test.dart';
import 'custom_painter_test.dart';
import 'inherited_widget_test.dart';

class UtilPage extends StatelessWidget {
  const UtilPage({super.key});

  @override
  Widget build(BuildContext context) {
    List<PageNavModel> utilCellItems = [
      PageNavModel('InheritedWidget测试', const InheritedWidgetTestPage()),
      PageNavModel('异步函数测试', const AsyncTestPage()),
      PageNavModel('动画测试', const AnimationTestPage()),
      PageNavModel('自定义绘制', const CustomPainterTestPage()),
    ];
    return Scaffold(
      appBar: AppBar(
        title: const Text('工具列表'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            buildListSection(context, '工具类测试', utilCellItems),
          ],
        ),
      ),
    );
  }
}
