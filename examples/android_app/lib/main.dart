import 'package:android_app/controllers/theme.dart';
import 'package:android_app/global.dart';
import 'package:flutter/material.dart';

import 'controllers/index.dart';

void main() async {
  await initMobileApp();
  initController();
  runApp(_App());
}

class _App extends StatelessWidget {
  const _App();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => MobileApp.router(
        themeMode: ThemeController.of.themeMode.value,
        theme: ThemeController.of.theme.value,
        darkTheme: ThemeController.of.darkTheme.value,
        config: ThemeController.of.config.value,
        router: initRouter(),
      ),
    );
  }
}
