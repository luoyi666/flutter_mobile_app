import 'package:flutter/material.dart';
import 'package:flutter_mobile_app/flutter_mobile_app.dart';

import 'router.dart';

void main() async {
  await initMobileApp();
  runApp(MobileApp.router(router: initRouter()));
}
