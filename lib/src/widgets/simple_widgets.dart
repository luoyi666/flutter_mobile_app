part of flutter_mobile_app;

/// 构建通用的[ListTile]组件
Widget buildCellWidget(
  BuildContext context, {
  required String title,
  bool dense = true,
  Widget trailing = arrowRightWidget,
  GestureTapCallback? onTap,
  Widget? page,
  Widget? leading,
  Color? tileColor,
}) {
  return ListTile(
    onTap: onTap == null && page == null
        ? null
        : () {
            if (onTap != null) {
              onTap();
            } else {
              context.push(page!);
            }
          },
    dense: dense,
    leading: leading,
    trailing: arrowRightWidget,
    tileColor: tileColor,
    title: Text(
      title,
      style: TextStyle(
        fontSize: Theme.of(context).listTileTheme.titleTextStyle?.fontSize,
      ),
    ),
  );
}

Widget buildListViewDemo({
  int? itemCount,
  ScrollPhysics? physics,
}) {
  return SuperListView.builder(
    itemCount: itemCount,
    physics: physics,
    itemBuilder: (context, index) => buildCellWidget(
      context,
      onTap: () {},
      title: '列表-${index + 1}',
    ),
  );
}

Widget buildCardWidget(
  BuildContext context, {
  String? title,
  List<Widget> children = const [],
}) {
  return Card(
    clipBehavior: Clip.hardEdge,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (title != null)
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 16, 16, 8),
            child: Text(title, style: context.h4),
          ),
        Column(children: children),
      ],
    ),
  );
}

Widget buildListSection(BuildContext context, String title, List<NavModel> navModel) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.min,
    children: [
      Padding(
        padding: const EdgeInsets.fromLTRB(16, 12, 16, 12),
        child: Text(
          title,
          style: Theme.of(context).textTheme.titleMedium,
        ),
      ),
      Column(
        children: navModel
            .map(
              (e) => Column(
                children: [
                  ListTile(
                    title: Text(e.title),
                    trailing: const Icon(Icons.keyboard_arrow_right_outlined),
                    onTap: () {
                      if (e is PageNavModel) {
                        context.push(e.page);
                      } else if (e is UrlNavModel) {
                        context.go(e.path);
                      }
                    },
                  ),
                  buildDividerWidget(context),
                ],
              ),
            )
            .toList(),
      )
    ],
  );
}
