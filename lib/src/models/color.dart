part of flutter_mobile_app;

enum ColorMode {
  auto,
  light,
  dark,
}

/// 颜色工具类
class ColorUtil {
  ColorUtil._();

  /// 返回一个动态颜色
  static Color dynamicColor(
    lightColor,
    darkColor,
    BuildContext context, {
    ColorMode? mode,
  }) {
    ColorMode colorMode = mode ?? ColorMode.auto;
    switch (colorMode) {
      case ColorMode.auto:
        return BrightnessWidget.isDark(context) ? darkColor : lightColor;
      case ColorMode.light:
        return lightColor;
      case ColorMode.dark:
        return darkColor;
    }
  }
}
