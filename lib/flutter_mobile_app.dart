library flutter_mobile_app;

import 'dart:async';

import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'flutter_mobile_app.dart';

export 'package:flutter_base/flutter_base.dart';
export 'package:flutter_router/flutter_router.dart';
export 'package:flutter_http/flutter_http.dart';

// 精简后的getx，只保留核心响应式状态管理
export 'package:mini_getx/mini_getx.dart';

// flutter官方collection库，扩展集合函数
export 'package:collection/collection.dart';

// 优化tab左右滑动，官方的不跟手
export 'package:extended_tabs/extended_tabs.dart';

part 'src/extensions/drawer.dart';

part 'src/extensions/modal.dart';

part 'src/commons/model.dart';

part 'src/utils/loading.dart';

part 'src/utils/serializable.dart';

part 'src/utils/toast.dart';

part 'src/models/color.dart';

part 'src/pages/simple_page.dart';

part 'src/widgets/exit_intercept.dart';

part 'src/widgets/hide_keyboard.dart';

part 'src/widgets/simple_widgets.dart';

part 'src/widgets/form/form.dart';

part 'src/widgets/form/form_item.dart';

part 'src/widgets/form/form_text_field.dart';

part 'src/widgets/cupertino/list_group.dart';

part 'src/widgets/cupertino/list_tile.dart';

/// 初始化App
Future<void> initApp({
  bool initFont = true,
  bool initHttpCache = true,
}) async {
  WidgetsFlutterBinding.ensureInitialized();
  DartBase.setLogger(level: DartUtil.isRelease ? null : Level.debug);
  await initStorage();
  // 初始化字体
  if (initFont) {
    await FlutterFont.init();
    await FlutterFont.initSystemFontWeight();
  }
  // 初始化http缓存拦截器
  if (initHttpCache) await CacheInterceptor.init();
}
